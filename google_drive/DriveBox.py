from pydrive.auth import GoogleAuth, AuthenticationError, RefreshError
from pydrive.drive import GoogleDrive

from main import Constants
from main.Constants import GD_FILE_DATE_TO, GD_FILE_DATE_FROM


class DriveBox:
    g_drive = None
    drive_instance = None
    gauth = None

    def __init__(self):
        self.g_drive = GoogleDrive(self.gauth)

    def __new__(cls, *args, **kwargs):
        if cls.drive_instance is None:
            cls.gauth = GoogleAuth()
            try:
                cls.gauth.CommandLineAuth()
            except (AuthenticationError, RefreshError) as e:
                print(e)

            cls.drive_instance = super(DriveBox, cls).__new__(cls)
        return cls.drive_instance

    def get_folder_by_name(self, title):
        list = self.g_drive.ListFile({'q':  "trashed=false and title contains '%s' "
                                            "and mimeType='application/vnd.google-apps.folder'" % title,
                                            'maxResults': 1}).GetList()
        if len(list) > 0:
            return list[0]
        else:
            return None

    def get_child_folder_by_name(self, title, parent_folder_id):
        query = "title contains '{}' and mimeType='application/vnd.google-apps.folder' and '{}' in parents and trashed=false".format(title, parent_folder_id)
        folder = self.g_drive.ListFile({'q': query}).GetList()
        if len(folder) > 0:
            return folder[0]
        else:
            return None

    def create_folder(self, folder_name, parent_folder_id):
        folder_metadata = {
            'title': folder_name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [{"kind": "connect_gd#fileLink", "id": parent_folder_id}]
        }

        folder = self.g_drive.CreateFile(folder_metadata)
        folder.Upload()

        return folder



    def get_files_from_folder(self, folder_id):
        return self.g_drive.ListFile({'q': "'%s' in parents and trashed=false" % folder_id}).GetList()

    def get_all_folder_from_folder(self, folder_id):
        return self.g_drive.ListFile({"q": "'%s' in parents and mimeType='application/vnd.google-apps.folder' and trashed=false" % folder_id}).GetList()

    # get list files from folder
    def list_files_from_parent_folder(self, folder_id):
        return self.g_drive.ListFile({'q': "modifiedDate>='{}' and "
                                           "modifiedDate<='{}' and "
                                           "'{}' in parents".format(GD_FILE_DATE_FROM, GD_FILE_DATE_TO, folder_id)}).GetList()

    def create_pdf_file(self, name_of_file, parent_folder_id, path_to_pdf):
        gd_file = self.g_drive.CreateFile({'title': name_of_file,
                                                   "parents": [
                                                       {"kind": "drive#fileLink", "id": parent_folder_id}]})

        gd_file.SetContentFile(path_to_pdf)
        gd_file.Upload()

        return gd_file

    def upload_attachmnet(self, attachment_name, path, folder):
        gd_file = self.g_drive.CreateFile({'title': attachment_name,
                                                  "parents": [
                                                      {"kind": "drive#fileLink", "id": folder['id']}]})
        gd_file.SetContentFile(path)
        gd_file.Upload()

        return gd_file

    # get xml white list
    def get_white_list_file(self, white_list_name):
        xmls_white_l = None
        xmls_list = self.g_drive.ListFile({'q': "'%s' in parents  and trashed=false" %
                                               Constants.ID_GD_WHITELIST_FOLDER}).GetList()

        if any(white_list_name.lower().replace(' ', '') in self.file['title'].lower().replace(' ', '')
               for self.file in xmls_list):
            xmls_white_l = self.file

        return xmls_white_l
from tempfile import NamedTemporaryFile

from google_drive.DriveBox import DriveBox
from main import Utils, Constants
from pdf.PdfBox import PdfBox


# CALL GENERATION PDF CLASS, DOWNLOAD PDF TO GOOGLE DRIVE
class PDFManager:
    def __init__(self, mail, attachment_url_list, date_for_name):
        self.gd_box = DriveBox()
        self.date_for_name = date_for_name
        self.mail = mail
        self.attachment_url_list = attachment_url_list
        self.pdf_box = PdfBox(mail=self.mail, attachments=self.attachment_url_list)

    def gen_mail_to_pdf(self):
        try:
            with NamedTemporaryFile(delete=False, suffix=".html") as tf:
                #print("create mail tmp html " + tf.name)
                self.pdf_mail_path = self.pdf_box.generation_mail_pdf(tmp_html_file=tf.name)

        except BaseException as e:
            print("gen_mail_to_pdf " + str(e))
            raise Exception

    def save_pdf_to_gd(self, gd_parent_folder_id):
        # save mail pdf
        try:

            name_mailpdf = self.generation_mailimage_name()

            if self.pdf_mail_path is not None:
                # print("success create pdf localy " + self.pdf_mail_path)
                mail_pdf = self.gd_box.create_pdf_file(name_of_file=name_mailpdf,
                                                       parent_folder_id=gd_parent_folder_id['id'],
                                                       path_to_pdf=self.pdf_mail_path)

                if mail_pdf.uploaded:
                   print("success upload mailimage " + name_mailpdf)
            else:
                print("error upload mailimage")

        except BaseException as e:
            print("save_pdf_to_gd " + str(e))

    def generation_mailimage_name(self):
        try:
            size_all_attachments = Utils.get_size_all_attachments(self.mail) + 1
            mail_from = Utils.generation_company_and_mailFrom_name(self.mail)

            name_mailpdf = Utils.change_name_of_file(current_date=self.date_for_name,
                                                     name_file=Constants.NAME_MAIL_PDF,
                                                     current_index=size_all_attachments,
                                                     all_size=size_all_attachments,
                                                     delivered_to=mail_from)

            return name_mailpdf
        except BaseException as e:
            print("generation_mailimage_name " + str(e))
            raise Exception

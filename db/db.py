import contextlib
import sqlite3

DB_NAME = "db/cip_bokoredopro.db"
TIMEOUT_DB = 600

table_hash = "Hash"

class DbBox:
    conn = None
    cur = None

    def __init__(self):
        sql_hash = 'create table if not exists ' + table_hash + ' (id INTEGER primary key AUTOINCREMENT, ' \
                                                                          'hash_file TEXT)'

        with contextlib.closing(
                sqlite3.connect(DB_NAME, check_same_thread=False, timeout=TIMEOUT_DB)) as conn:  # auto-closes
            with conn:  # auto-commits
                with contextlib.closing(conn.cursor()) as cur:  # auto-closes
                    cur.execute(sql_hash)

    def add_new_hash(self, hash_file):
        if len(self.get_value_by_hash(hash_file)) == 0:
            with contextlib.closing(sqlite3.connect(DB_NAME, check_same_thread=False, timeout=TIMEOUT_DB)) as conn:  # auto-closes
                with conn:  # auto-commits
                    with contextlib.closing(conn.cursor()) as cur:  # auto-closes
                        cur.execute(
                            'INSERT INTO Hash(hash_file) VALUES(?)',
                            (hash_file,))
                        conn.commit()
                        return cur.lastrowid

    def get_all_hash(self):
        hash_list = None
        with contextlib.closing(
                sqlite3.connect(DB_NAME, check_same_thread=False, timeout=TIMEOUT_DB)) as conn:  # auto-closes
            with conn:  # auto-commits
                with contextlib.closing(conn.cursor()) as cur:  # auto-closes
                    cur.execute('SELECT * FROM Hash')
                    hash_list = cur.fetchall()
        return hash_list

    def get_value_by_hash(self, hash_file):
        hash_list = []
        with contextlib.closing(sqlite3.connect(DB_NAME, check_same_thread=False, timeout=TIMEOUT_DB)) as conn:  # auto-closes
            with conn:  # auto-commits
                with contextlib.closing(conn.cursor()) as cur:  # auto-closes
                    cur.execute(
                        'SELECT * FROM Hash WHERE hash_file=?', (hash_file,)
                    )
                    conn.commit()
                    hash_list = cur.fetchall()

        return hash_list
import hashlib
from tempfile import NamedTemporaryFile

from db.db import DbBox


class HashFileWorker:

    def __init__(self):
        self.db = DbBox()

    def md5(self, fname):
        return hashlib.md5(open(fname, 'rb').read()).hexdigest()
        # hash_md5 = hashlib.md5()
        # with open(fname, "rb") as f:
        #     for chunk in iter(lambda: f.read(4096), b""):
        #         hash_md5.update(chunk)
        # return hash_md5.hexdigest()

    # def get_hash_summ_from_file(self, part):
    #     with NamedTemporaryFile(delete=False) as tf:
    #         tf.write(part.get_payload(decode=True))
    #         tf.flush()
    #
    #         return self.md5(tf.name), tf.name

    def get_hash_summ_from_payload(self, payload):
        with NamedTemporaryFile(delete=True) as tf:
            tf.write(payload)
            tf.flush()

            return self.md5(tf.name)

    def create_hash_summ_list(self, list):
        print('create_hash_summ_list')
        hash_summ_lsit = []
        for item in list:
            checksum = item.get('md5Checksum', 'no checksum')
            hash_summ_lsit.append(checksum)
            self.db.add_new_hash(checksum)

        return hash_summ_lsit

    def check_hash_sum(self, filename, hash_sum, hash_summ_list, status_exist_in_gd, log_status_attachments):
        try:
            if hash_sum in hash_summ_list:
                # print('exist ' + filename)
                log_status_attachments.append('exist ' + str(filename))
                status_exist_in_gd.append(True)
            else:
                # print('not exist ' + filename)
                log_status_attachments.append('not exist ' + str(filename))
                status_exist_in_gd.append(False)

            return status_exist_in_gd, log_status_attachments

        except BaseException as e:
            print("check_hash_sum " + str(e))

    def check_hash_sum_file(self, hash_sum, hash_summ_list):
        try:
            if hash_sum in hash_summ_list:
                return True
            else:
                return False
        except BaseException as e:
            print("check_hash_sum " + str(e))

import tempfile
from tempfile import NamedTemporaryFile

import openpyxl
import os
import xlsxwriter as xlsxwriter
from openpyxl.styles import Font, Alignment
from openpyxl import load_workbook

from google_drive.DriveBox import DriveBox
from main import Constants

GLOBAL_LOG_FONT = Font(name='Roboto', size=8)
COMPANY_LOG_FONT = Font(name='Calibri', size=10)

class XmslBox:

    def get_mimetype_xlsm(self):
        mimetypes = {
            'application/vnd.google-apps.document': 'application/pdf',
            'application/vnd.google-apps.spreadsheet': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        }
        return mimetypes


    def get_while_list(self):
        try:
            drive = DriveBox()
            gd_white_list = drive.get_white_list_file(Constants.NAME_OF_WHITE_LIST)
            white_list = {}

            try:
                if gd_white_list is not None:
                    download_mimetype = None

                    if gd_white_list['mimeType'] in self.get_mimetype_xlsm():
                        download_mimetype = self.get_mimetype_xlsm()[gd_white_list['mimeType']]
                        prefix_file = os.path.splitext(os.path.basename(gd_white_list['title']))[1]

                        if ".xlsx" in prefix_file:
                            gd_white_list.GetContentFile(gd_white_list['title'], mimetype=download_mimetype)
                        else:
                            gd_white_list.GetContentFile(gd_white_list['title'] + ".xlsx", mimetype=download_mimetype)

                        wb = load_workbook(Constants.NAME_OF_WHITE_LIST + ".xlsx")

                        sheet = wb.worksheets[0]

                        i = 2
                        for cellObj in sheet['A2':'A' + str(len(sheet['A']))]:
                            for cell in cellObj:
                                if cell.value is not None:
                                    print(str(cell.value) + " " + str(sheet['D' + str(i)].value))
                                    inf_company = []
                                    inf_company.append(str(sheet['D' + str(i)].value))
                                    inf_company.append(str(sheet['C' + str(i)].value))

                                    # get all forward emails
                                    forward_emails = {}
                                    forward_email = str(sheet['D' + str(i)].value).split()
                                    forward_emails["forward_emails"] = forward_email
                                    inf_company.append(forward_email)

                                    white_list[str(cell.value.lower())] = inf_company
                            i = i + 1
                else:
                    print("cant get white list")

            except BaseException as e:
                print(e)

            return white_list

        except BaseException as e:
            print(e)
NAME_MAIL_PDF = "Mailimage.pdf"
DATE_FORMAT = '%Y%m%d%H%M%S'
IGNORE_TEXT = "Ta bort denna text om du vill lämna ett meddelande"
TRASH_FILES = ['txt', 'html', 'htm']
EXCLUDE_TEXT_IN_TXT_FILES = ["Skickat från min iPhone", "Sent from my iPhone"]

FOLDER_CHECK_IF_PROCESSED = 'Check_if_processed'

# DATE_MAIL_START = '(SINCE "24-Sep-2018" BEFORE "14-Oct-2018")'
DATE_MAIL_START = '(SINCE "30-Oct-2018" BEFORE "06-Nov-2018")'
GD_FILE_DATE_FROM = '2018-10-25T00:00:00'
GD_FILE_DATE_TO = '2018-11-08T00:00:00'

PREFIX_FOLDER = "@bokoredo.pro"

ID_GD_WHITELIST_FOLDER = '1sq3RplIyUxOKUN_tFBtRWGZtnmXOdDT-'
NAME_OF_WHITE_LIST = 'Companies'

CIP_FOLDER ='1f2Ihk_AT1JNwgFewLXXz7AihHZbMuH5d'

SYMBOL_PARSING_COMPANY_NAME = '+'

import os

from openpyxl import load_workbook

from google_drive.DriveBox import DriveBox


class LogWorker:

    def __init__(self):
        self.g_drive = DriveBox()

    def search_local_log(self, name_of_company):
        logs = []
        folder_name = name_of_company
        folder = self.g_drive.get_folder_by_name(folder_name)
        if folder:
            print(folder['title'])
            list_files_root_folder = self.g_drive.get_files_from_folder(folder['id'])
            if list_files_root_folder:
                name_of_log = "Logs" + " " + name_of_company.split('@')[0]
                if name_of_log in (self.file['title'] for self.file in list_files_root_folder):
                    print(self.file['title'])
                    logs = self.get_all_value_from_log(file=self.file)
        else:
            print("cant find folder " + folder_name)

        return logs

    def get_mimetype_xlsm(self):
        mimetypes = {
            'application/vnd.google-apps.document': 'application/pdf',
            'application/vnd.google-apps.spreadsheet': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        }
        return mimetypes

    def get_all_value_from_log(self, file):
        path_log = self.generation_log_file(file)
        log = self.read_xlsx(path_log)
        self.del_local_log_local(path_log)
        return log

    def read_xlsx(self, path):
        wb = load_workbook(path)
        sheet = wb.worksheets[0]
        log = []
        for cellObj in sheet['B2':'B' + str(len(sheet['B']))]:
            for cell in cellObj:
                if cell.value is not None:
                    log.append(cell.value)
        return log

    def generation_log_file(self, local_log):
        download_mimetype = self.get_mimetype_xlsm()[local_log['mimeType']]
        prefix_file = os.path.splitext(os.path.basename(local_log['title']))[1]

        if ".xlsx" in prefix_file:
            title = local_log['title']
            local_log.GetContentFile(local_log['title'], mimetype=download_mimetype)
        else:
            title = local_log['title'] + ".xlsx"
            local_log.GetContentFile(local_log['title'] + ".xlsx", mimetype=download_mimetype)

        path_tmp_file = title

        return path_tmp_file

    def del_local_log_local(self, path):
        os.remove(path)
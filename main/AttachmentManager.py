import email
import threading
import time
from concurrent.futures import ThreadPoolExecutor
from tempfile import NamedTemporaryFile

from tnefparse import TNEF

from main import Constants, Utils
from main.Constants import CIP_FOLDER

lock = threading.Lock()


class AttachmentManager:

    def __init__(self, mail, hash_worker, hash_summ_list, drive, log_list):
        self.mail = mail
        self.hash_worker = hash_worker
        self.hash_summ_list = hash_summ_list
        self.links_attachment = []
        self.drive = drive
        self.work_folder = None
        self.is_movie_mail = False
        self.date_for_name = time.strftime(Constants.DATE_FORMAT)
        self.log_list = log_list
        # self.date_for_name = "delete_this"
        self.__singl_processing_mail_walk(mail)

        if self.is_movie_mail:
            if self.add_mail_pdf:
                from pdf.PDFManager import PDFManager
                self.pdf_worker = PDFManager(mail=self.mail, attachment_url_list=self.links_attachment, date_for_name=self.date_for_name)
                self.pdf_worker.gen_mail_to_pdf()
                self.pdf_worker.save_pdf_to_gd(self.work_folder)

    def __singl_processing_mail_walk(self, mail):
        self.count_attachment = 0
        self.add_mail_pdf = Utils.check_add_mail_pdf(self.mail)

        with ThreadPoolExecutor(4) as executor:
            for _ in executor.map(self.__singl_processing_part_mail, mail.walk()):
                pass

    def __singl_processing_part_mail(self, part):
        try:
            if part.get_content_maintype() == 'multipart':
                return

            if part.get('Content-Disposition') is None:
                return

            if self.additional_check_its_multipart_or_not(part) is None:
                return

            if Utils.additional_check_its_trash_or_not(part) is None:
                return

            # check .dat files
            new_attachments = self.additional_check_for_dat_files(part)
            if len(new_attachments) > 0:
                for attachment in new_attachments:
                    hash_sum = self.hash_worker.get_hash_summ_from_payload(attachment[1])
                    is_exist_in_gd = self.hash_worker.check_hash_sum_file(hash_sum=hash_sum,
                                                                          hash_summ_list=self.hash_summ_list)
                    if not is_exist_in_gd:
                        gen_file_name = Utils.get_name_of_file(str(attachment[0]))
                        print("hash not exist " + str(gen_file_name))

                        if not self.check_exist_file(str(gen_file_name)):

                            self.work_folder = self.get_work_folder(self.mail)
                            print('not exist file .dat', part.get_filename())

                            new_current_file_hash = self.hash_worker.get_hash_summ_from_payload(
                                payload=part.get_payload(decode=True))
                            new_file_list = self.drive.get_files_from_folder(self.work_folder['id'])
                            new_hash_list_files = self.hash_worker.create_hash_summ_list(new_file_list)

                            if new_current_file_hash not in new_hash_list_files:
                                print('not exist file .dat ', attachment[0])
                                self.upload_attachent(attachment[1], attachment[0], self.work_folder)
                                self.is_movie_mail = True
                            else:
                                print('already exist .dat ' + attachment[0])
                        else:
                            print("hash not exist but log exist " + (str(attachment[0])))

            else:
                hash_sum = self.hash_worker.get_hash_summ_from_payload(part.get_payload(decode=True))
                is_exist_in_gd = self.hash_worker.check_hash_sum_file(hash_sum=hash_sum,
                                                                      hash_summ_list=self.hash_summ_list)
                if not is_exist_in_gd:
                    gen_file_name = Utils.get_name_of_file(str(part.get_filename()))
                    print("hash not exist " + str(gen_file_name))

                    if not self.check_exist_file(str(gen_file_name)):

                        self.work_folder = self.get_work_folder(self.mail)

                        new_current_file_hash = self.hash_worker.get_hash_summ_from_payload(payload=part.get_payload(decode=True))
                        new_file_list = self.drive.get_files_from_folder(self.work_folder['id'])
                        new_hash_list_files = self.hash_worker.create_hash_summ_list(new_file_list)

                        if new_current_file_hash not in new_hash_list_files:
                            print('not exist file ', part.get_filename())
                            self.upload_attachent(part.get_payload(decode=True), part.get_filename(), self.work_folder)
                            self.is_movie_mail = True
                        else:
                            print('already exist ' + part.get_filename())

                    else:
                        print("hash not exist but log exist " + (str(part.get_filename())))

        except BaseException as e:
            print("__singl_processing_part_mail" + str(e))
            return

    def check_exist_file(self, filename):
        if any(filename.lower() in log.lower() for log in self.log_list):
            return True
        else:
            return False

    def get_work_folder(self, mail):
        # get main folder date
        main_folder_name = Utils.get_current_date()
        main_folder = self.find_or_create_folder(main_folder_name, CIP_FOLDER)

        # get company folder
        company_name = Utils.get_main_company(Utils.get_mail_delivered_to(mail))
        company_folder = self.find_or_create_folder(company_name, main_folder['id'])

        # check and get company prefix folder
        company_prefix_name = Utils.get_main_company_prefix(Utils.get_mail_delivered_to(mail))
        if company_prefix_name is not None:
            company_prefix_folder = self.find_or_create_folder(company_prefix_name, company_folder['id'])
            company_folder = company_prefix_folder

        # get mail folder
        mail_name_foder = Utils.generate_mail_name_folder(mail)
        mail_folder = self.find_or_create_folder(mail_name_foder, company_folder['id'])
        work_folder = mail_folder

        return work_folder

    def upload_attachent(self, payload, filename, folder):
        try:
            print('try upload_attachent')

            self.count_attachment += 1
            attachment_name = self.generation_attachmnet_name(filename, self.count_attachment)

            with NamedTemporaryFile(delete=True) as tf:
                tf.write(payload)
                tf.flush()
                tmp_name = tf.name

                gd_file = self.drive.upload_attachmnet(attachment_name, path=tmp_name,
                                                       folder=folder)
                if gd_file.uploaded:
                    self.links_attachment.append(gd_file["alternateLink"])
                    print("uploaded " + attachment_name)

                else:
                    print("not uploaded " + attachment_name)
        except BaseException as e:
            print("upload_attachent " + str(e))

    def find_or_create_folder(self, folder_name, parent_folder_id):
        try:
            lock.acquire()
            try:
                folder = self.drive.get_child_folder_by_name(folder_name, parent_folder_id)
                if folder is None:
                    print('create folder ' + folder_name)
                    folder = self.drive.create_folder(folder_name, parent_folder_id)
                else:
                    print('find folder ' + folder_name)

                return folder
            except BaseException as e:
                print(e)
            finally:
                lock.release()
        except BaseException as e:
            print("find_or_create_folder " + str(e))

    def generation_attachmnet_name(self, file_name, count):
        try:
            if self.add_mail_pdf:
                size_all_attachments = Utils.get_size_all_attachments(self.mail) + 1
            else:
                size_all_attachments = Utils.get_size_all_attachments(self.mail)

            mail_from = Utils.generation_company_and_mailFrom_name(self.mail)

            attachment_name = Utils.change_name_of_file(current_date=self.date_for_name,
                                                            name_file=file_name, current_index=count,
                                                            all_size=size_all_attachments,
                                                            delivered_to=mail_from)

            return attachment_name

        except BaseException as e:
            print("generation_attachmnet_name " + str(e))

    def additional_check_for_dat_files(self, part):
        new_attachments = []
        filename_orig = str(part.get_filename())
        if filename_orig and filename_orig.strip().lower() in ['winmail.dat', 'win.dat']:
            try:
                winmail = TNEF(part.get_payload(decode=True))
                for attach in winmail.attachments:
                    new_attachments.append((self.get_filename(attach.name.decode("utf-8")), attach.data))
            except BaseException as e:
                print(e)

        return new_attachments

    def get_filename(self, filename_orig):
        filename = ''
        try:
            filename = self.decode_mime_words(filename_orig).lower().replace(' ', '').encode(
                "ascii", errors="ignore").decode()
        except BaseException as e:
            filename = 'none'
            print("try decode_mime_words " + str(e))
        return filename

    def additional_check_its_multipart_or_not(self, file):
        try:
            return self.decode_mime_words(file.get_filename().encode("ascii", errors="ignore").decode())
        except BaseException as e:
            return None

    # need refactor, it method need remove to utils
    def decode_mime_words(self, s):
        try:
            return u''.join(
                word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
                for word, encoding in email.header.decode_header(s))
        except BaseException as e:
            return s
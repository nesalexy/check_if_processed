import datetime
import email
import re


class Utils:

    def __init__(self):
        self.ADDR_PATTERN = re.compile('<(.*?)>')

    def count_status(self, data):
        i = 0
        for status in data:
            if status is True:
                i += 1
        return i

    # need refactor, it method need remove to utils
    def decode_mime_words(self, s):
        return u''.join(
            word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
            for word, encoding in email.header.decode_header(s))

        # get main company name

    def get_mail_delivered_to(self, mail):
        mail_delivered_to = ''
        try:
            mail_delivered_to = self.decode_mime_words(self.get_delivered_to(mail)).lower().replace(' ',
                                                                                                                '').encode(
                "ascii", errors="ignore").decode()
        except BaseException as e:
            mail_delivered_to = ''

        return mail_delivered_to

    def get_delivered_to(self, mail):
        addr_fields = ['Delivered-To']
        for f in addr_fields:
            rfield = mail.get(f, "")
            if len(rfield) > 0:
                array_inf = rfield.replace("\r\n\t", " ").split(' ')
                for inf in array_inf:
                    email = inf.replace("<", "").replace(">", "").replace("\"", "").replace("\r\n\t", " ")
                    match = re.search(r'[\w.-]+@[\w.-]+.\w+', email)
                    if match:
                        return email

    def get_main_company(self, pars_mail):
        arr_company_name = pars_mail.replace('@', ' ').split()
        arr_company_name_and_prefix = self.__replace_main_name_and_prefix(arr_company_name[0])

        return arr_company_name_and_prefix[0]

        # parsing main company name from mail address

    def get_main_company_prefix(self, pars_mail):
        arr_company_name = pars_mail.replace('@', ' ').split()
        arr_company_name_and_prefix = self.__replace_main_name_and_prefix(arr_company_name[0])

        if len(arr_company_name_and_prefix) > 1:
            return arr_company_name_and_prefix[1]
        else:
            return None

    def generate_mail_name_folder(self, mail):
        name_folder = ""
        mail_delivered_to = ''
        try:
            mail_delivered_to = self.decode_mime_words(self.get_delivered_to(mail)).lower().replace(' ', '').encode(
                "ascii", errors="ignore").decode()
        except BaseException as e:
            mail_delivered_to = ''

        mail_from = ''
        try:
            mail_from = self.decode_mime_words(self.get_mail_from(mail)).lower().replace(' ', '').encode(
                "ascii", errors="ignore").decode()
        except BaseException as e:
            mail_from = ''

        mail_subject = ''
        try:
            mail_subject = self.decode_mime_words(self.get_subject_from_mail(mail)).lower().replace(' ',
                                                                                                                '').encode(
                "ascii", errors="ignore").decode()
        except BaseException as e:
            mail_subject = ''

        if len(mail_subject.replace(" ", "")) == 0:
            mail_subject = 'without_subject'

        name_folder = mail_from + "_" + mail_subject + "_" + mail_delivered_to
        return name_folder

    def get_mail_from(self, mail):
        mail_from = re.findall(self.ADDR_PATTERN, mail['From'])
        if len(mail_from) == 0:
            mail_from.append(mail['From'])

        return mail_from[0]

    def __replace_main_name_and_prefix(self, full_company_name):
        main_company_name = []
        if '+' in full_company_name:
            main_company_name = full_company_name.replace('+', ' ').split()
        else:
            main_company_name.append(full_company_name)

        return main_company_name

    def get_current_date_time(self):
        now = datetime.datetime.now()
        return now.strftime("%d-%b %H:%M")

    def generation_exist_list_files(self, gd_list_exist, gd_list_delete):
        final_required_list = set(gd_list_exist) - set(gd_list_delete)
        return final_required_list

    def get_current_date(self):
        now = datetime.datetime.now()
        return now.strftime("%d-%b")

    def get_subject_from_mail(self, mail):
        subject = ""
        try:
            subject = self.decode_mime_words(mail["Subject"].encode("ascii", errors="ignore").decode())
        except BaseException as e:
            print(e)

        return subject

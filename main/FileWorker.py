import os

from main.utils import Utils


class FileWorker:

    def __init__(self):
        self.utils = Utils

    def remove_file(self, file_path):
        try:
            print("remove " + file_path)
            os.remove(file_path)
        except BaseException as e:
            print(e)

    def get_part_name(self, part):
        filename = str(part.get_filename())
        decode_filename = self.utils.decode_mime_words(filename).lower().replace(' ', '').encode(
                                                "ascii", errors="ignore").decode()

        return decode_filename

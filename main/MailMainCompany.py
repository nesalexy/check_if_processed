import re

from main import Utils, Constants

ADDR_PATTERN = re.compile('<(.*?)>')
EXCLUDE_EMAILS = ['root@mx.bokoredo.pro',   'clients@mx.bokoredo.pro',
                  'app@i-script.pl',        'clients@bokoredo.pro',
                  'root@mx.bokoredo.pro',   'victor.b.ysbm@gmail.com']

class MailMainCompany:

    def get_list_mails(self, msg_parsed, white_list):
        recipients = []
        to_cc_list = []

        try:
            prioritet_field = 'Bcc'
            addr_fields = ['Delivered-To', 'To', 'Cc']

            # get bcc(it can be not only bcc) from recived header email
            recipients = self.get_mails_from_recived(msg_parsed, white_list, recipients)
            if len(recipients) > 0:
                return recipients

            # get all delivered to
            recipients = self.get_all_delivered_to(msg_parsed, white_list, recipients)

            # check priority field
            rfield = msg_parsed.get(prioritet_field, "")
            if len(rfield) > 0:
                # create only bcc
                recipients = self.add_emails_to_list(msg_parsed, rfield, recipients, white_list)
                # check exist to cc list
                for f in addr_fields:
                    rfield = msg_parsed.get(f, "")
                    if len(rfield) > 0:
                        to_cc_list = self.add_emails_to_list(msg_parsed, rfield, to_cc_list, white_list)

                return recipients
            else:
                for f in addr_fields:
                    rfield = msg_parsed.get(f, "")
                    if len(rfield) > 0:
                        recipients = self.add_emails_to_list(msg_parsed, rfield, recipients, white_list)

        except BaseException as e:
            print("get_list_mails", str(e))

        return recipients

    # get bcc mail from recived
    def get_mails_from_recived(self, msg_parsed, white_list, recipients):
        try:
            # get all received
            recived = []
            for key, value in msg_parsed.items():
                if key == 'Received':
                    recived.append(value)

            # parsing line and get mail
            for line in recived:
                rlist = re.findall(ADDR_PATTERN, line)
                if len(rlist) > 0:
                    for email in rlist:
                        if email.lower() not in EXCLUDE_EMAILS:
                            if email.lower() not in (already_exist_mail.lower() for already_exist_mail in recipients):
                                if Utils.is_company_in_white_list(white_list, email.replace('@', ' ').split()[0].lower()):
                                    recipients.append(email)


            return recipients
        except BaseException as e:
            print("get_mails_from_recived", str(e))

    # get all delivered to
    def get_all_delivered_to(self, msg_parsed, white_list, recipients):
        try:
            # get all delivered-to
            delivered_to = []
            for key, value in msg_parsed.items():
                if key == 'Delivered-To':
                    delivered_to.append(value)

            for line in delivered_to:
                recipients = self.add_emails_to_list(msg_parsed, line, recipients, white_list)

            return recipients
        except BaseException as e:
            print("get_all_delivered_to", str(e))

    # add mails to list who exist in white list
    def add_emails_to_list(self, msg_parsed, rfield, recipients, white_list):
        try:
            array_inf = rfield.replace("\r\n\t", " ").split(' ')
            for inf in array_inf:
                email = inf.replace("<", "").replace(">", "").replace("\"", "").replace("\r\n\t", " ")
                match = re.search(r'[\w.-]+@[\w.-]+.\w+', email)
                if match:
                    if email.lower() not in EXCLUDE_EMAILS:
                        if email.lower() not in (already_exist_mail.lower() for already_exist_mail in recipients):
                            checkEmail = email.replace('@', ' ').split()[0].lower();
                            if Utils.is_company_in_white_list(white_list, checkEmail):
                                recipients.append(email)
                            # aditional check whithout prefix
                            elif len(self.replace_main_name_and_prefix(email.replace('@', ' ').split()[0].lower())) > 1:
                                company = self.replace_main_name_and_prefix(email.replace('@', ' ').split()[0].lower())
                                if Utils.is_company_in_white_list(white_list, company[0]):
                                    recipients.append(email)

        except BaseException as e:
            try:
                print("From: " + msg_parsed['From'] + " To: " + msg_parsed['Delivered-To'] + " " + " Date: " + msg_parsed['Date'])
            except BaseException as e1:
                pass
            print("add_emails_to_list", str(e))

    def replace_main_name_and_prefix(self, full_company_name):
        main_company_name = []
        if Constants.SYMBOL_PARSING_COMPANY_NAME in full_company_name:
            main_company_name = full_company_name.replace(Constants.SYMBOL_PARSING_COMPANY_NAME, ' ').split()
        else:
            main_company_name.append(full_company_name)

        return main_company_name

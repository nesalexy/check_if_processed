import email
import mmap
import random
import re
import smtplib
import string
import tempfile
import datetime

import os
import time
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formataddr, formatdate

from bs4 import BeautifulSoup
from tnefparse import TNEF

from main import Constants

ADDR_PATTERN = re.compile('<(.*?)>')


def get_current_date_for_log():
    now = datetime.datetime.now()
    return now.strftime("%d-%b %H:%M")


def get_current_worksheet_name():
    now = datetime.datetime.now()
    return now.strftime("%b %y")


def get_date_for_xlsm(date):
    date_tuple = email.utils.parsedate_tz(date)
    date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
    return (datetime.datetime.strptime(str(date),
                                       '%Y-%m-%d %H:%M:%S')
            .strftime('%d-%b %H:%M'))


def get_date_for_mail(date):
    # October 24, 2017 at 12:49 PM EEST
    convert_date = ''
    try:
        date_tuple = email.utils.parsedate_tz(date)
        date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
        convert_date = (datetime.datetime.strptime(str(date),
                                           '%Y-%m-%d %H:%M:%S')
                .strftime('%B %d, %Y at %I:%M %Z'))
    except BaseException as e:
        pass

    return convert_date

def get_date_for_month_folder():
    today = datetime.date.today()
    first = today.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)
    return lastMonth.strftime('%y%m')


def get_size_all_attachments(mail):
    size = 0
    for part in mail.walk():
        if part.get_content_maintype() == 'multipart': continue
        if part.get('Content-Disposition') is None: continue
        try:
            decode_mime_words(part.get_filename().encode("ascii", errors="ignore").decode())
        except BaseException as e: continue

        if additional_check_its_trash_or_not(part) is None: continue

        # mayby mail contains .dat files(like zip but for mail)
        additional_size = len(additional_check_for_dat_files(part))
        if additional_size > 0:
            size += additional_size
        else:
            size += 1

    return size


def additional_check_its_trash_or_not(part):
    try:
        attachmnet_name = part.get_filename()
        payload = part.get_payload(decode=True)
        attachentnt_prefix = get_prefix_from_name(attachmnet_name)

        if attachentnt_prefix in Constants.TRASH_FILES:
            if attachentnt_prefix in 'htm':
                print(attachentnt_prefix + " " + "Trash")
                return None
            else:
                try:
                    with tempfile.NamedTemporaryFile(delete=True) as tf:
                        tf.write(payload)
                        tf.flush()
                        path = tf.name
                        if os.stat(path).st_size == 0:
                            print(attachentnt_prefix + " " + "Trash")
                            return None
                        else:
                            with open(path, 'rb', 0) as file, \
                                    mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
                                for exclude_text in Constants.EXCLUDE_TEXT_IN_TXT_FILES:
                                    if s.find(str.encode(exclude_text)) != -1:
                                        print(attachentnt_prefix + " " + str(exclude_text))
                                        return None

                        return True

                except BaseException as e:
                    print(e)
                    return None
        else:
            return True

    except BaseException as e:
        print(e)
        return None


def decode_mime_words(s):
    return u''.join(
        word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
        for word, encoding in email.header.decode_header(s))


def get_mail_from(mail):
    mail_f = ""
    try:
        mail_from = re.findall(ADDR_PATTERN, mail['From'])
        if len(mail_from) == 0:
            mail_from.append(mail['From'])

        mail_f = decode_mime_words(mail_from[0].encode("ascii", errors="ignore").decode())
    except BaseException as e:
        print(e)

    return mail_f


def get_subject_from_mail(mail):
    subject = ""
    try:
        subject = decode_mime_words(mail["Subject"].encode("ascii", errors="ignore").decode())
    except BaseException as e:
        print(e)

    return subject


def get_mails_list(get_mails):
    mails = ""
    try:
        if get_mails is not None:
            mail_to_list = re.findall(ADDR_PATTERN, get_mails.encode("ascii", errors="ignore").decode())
            if len(mail_to_list) > 0:
                for mail in mail_to_list:
                    mails += mail + " "
            else:
                mails = get_mails.encode("ascii", errors="ignore").decode()

            return mails

    except BaseException as e:
        pass
    return mails


def clear_tmp_folder():
    try:
        print("clear tmp folder")
        tmp_path = tempfile.gettempdir()
        list_files = os.listdir(tmp_path)
        for file in list_files:
            try:
                os.remove(tmp_path + "/" + file)
            except BaseException as e:
                pass
    except BaseException as e:
        print(e)


def remove_file(file_path):
    try:
        print("remove " + file_path)
        os.remove(file_path)
    except BaseException as e:
        print(e)


def get_random_name():
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(12)])


def iter_rows(ws):
    for row in ws.iter_rows():
        yield [cell for cell in row]

# generation new name of file
def get_name_of_file(name_file):
    # filename can be contains norwegian charset, need decode
    try:
        decode_filename = decode_mime_words(name_file.encode("ascii", errors="ignore").decode())
    except BaseException as e:
        decode_filename = ""


# generation new name of file
def change_name_of_file(current_date, name_file, current_index, all_size, delivered_to):
    # filename can be contains norwegian charset, need decode
    try:
        decode_filename = decode_mime_words(name_file.encode("ascii", errors="ignore").decode())
    except BaseException as e:
        decode_filename = ""

    # current_date = time.strftime(Constants.DATE_FORMAT)

    return current_date + "_" \
           + str(current_index) + "of" + str(all_size) + "_" \
           + delivered_to + "_" \
           + decode_filename

def get_name_of_file(name_file):
    try:
        decode_filename = decode_mime_words(name_file.encode("ascii", errors="ignore").decode())
    except BaseException as e:
        decode_filename = None

    return decode_filename


# check contains company in list or not
def is_company_in_white_list(white_list, company):
    if company in white_list:
        return True
    else:
        return False


def get_prefix_from_name(filename):
    prefix = ""
    try:
        prefix = filename.rsplit('.', 1)[-1].lower()
    except BaseException as e:
        prefix = "jpg"
    return prefix


def add_message_to_move_list(email_id, mail_folder, list_mails_movie):
    if email_id not in list_mails_movie:
        inf = []
        inf.append(mail_folder)
        list_mails_movie[email_id] = inf


def additional_check_for_dat_files(part):
    new_attachments = []
    try:
        filename_orig = str(part.get_filename())
        if filename_orig and filename_orig.strip().lower() in ['winmail.dat', 'win.dat']:
            try:
                winmail = TNEF(part.get_payload(decode=True))
                for attach in winmail.attachments:
                    new_attachments.append((get_name_of_file(attach.name.decode("utf-8")), attach.data))
            except BaseException as e:
                raise Exception

    except BaseException as e:
        print(e)

    return new_attachments


def get_current_date_time():
    now = datetime.datetime.now()
    return now.strftime("%d-%b %H:%M")


def get_current_date():
    now = datetime.datetime.now()
    return now.strftime("%d-%b")


def get_main_company(pars_mail):
    arr_company_name = pars_mail.replace('@', ' ').split()
    arr_company_name_and_prefix = replace_main_name_and_prefix(arr_company_name[0])

    return arr_company_name_and_prefix[0]


def get_delivered_to(mail):
    email = ''
    addr_fields_del_to = ['Delivered-To', 'To']

    for f in addr_fields_del_to:
        rfield = mail.get(f, "")
        if len(rfield) > 0:
            array_inf = rfield.replace("\r\n\t", " ").split(' ')
            for inf in array_inf:
                email = inf.replace("<", "").replace(">", "").replace("\"", "").replace("\r\n\t", " ")
                match = re.search(r'[\w.-]+@[\w.-]+.\w+', email)
                if match:
                    if email not in 'clients@bokoredo.pro':
                        return email



def replace_main_name_and_prefix(full_company_name):
    main_company_name = []
    if '+' in full_company_name:
        main_company_name = full_company_name.replace('+', ' ').split()
    else:
        main_company_name.append(full_company_name)

    return main_company_name


def get_mail_delivered_to(mail):
    mail_delivered_to = ''
    try:
        mail_delivered_to = decode_mime_words(get_delivered_to(mail)).lower().replace(' ',
                                                                                                                '').encode(
                "ascii", errors="ignore").decode()
    except BaseException as e:
        mail_delivered_to = ''

    return mail_delivered_to


def get_main_company_prefix(pars_mail):
    arr_company_name = pars_mail.replace('@', ' ').split()
    arr_company_name_and_prefix = replace_main_name_and_prefix(arr_company_name[0])

    if len(arr_company_name_and_prefix) > 1:
        return arr_company_name_and_prefix[1]
    else:
        return None


def generate_mail_name_folder(mail):
    name_folder = ""
    mail_delivered_to = ''
    try:
        mail_delivered_to = decode_mime_words(get_delivered_to(mail)).lower().replace(' ', '').encode(
                "ascii", errors="ignore").decode()
    except BaseException as e:
        mail_delivered_to = ''

    mail_from = ''
    try:
        mail_from = decode_mime_words(get_mail_from(mail)).lower().replace(' ', '').encode(
                "ascii", errors="ignore").decode()
    except BaseException as e:
        mail_from = ''

    mail_subject = ''
    try:
        mail_subject = decode_mime_words(get_subject_from_mail(mail)).lower().replace(' ',
                                                                                                                '').encode(
                "ascii", errors="ignore").decode()
    except BaseException as e:
        mail_subject = ''

    if len(mail_subject.replace(" ", "")) == 0:
        mail_subject = 'without_subject'

    name_folder = mail_from + "_" + mail_subject + "_" + mail_delivered_to

    return name_folder


def generation_company_and_mailFrom_name(mail):
    delivered_to = get_delivered_to(mail)
    return delivered_to.replace('@', ' ').split()[0] + '_' + get_mail_from(mail)


def check_add_mail_pdf(mail):
    add_mail_pdf = True
    try:
        for part in mail.walk():
            if part.get_content_type() == "text/plain":
                soup = BeautifulSoup(part.get_payload(decode=True).decode('utf-8', errors='ignore'), "lxml")
                html_content = ''.join(['%s' % x for x in soup.body.contents])

                if Constants.IGNORE_TEXT.encode("ascii", errors="ignore").decode() \
                        in html_content.encode("ascii", errors="ignore").decode():
                    add_mail_pdf = False
                    break
                else:
                    add_mail_pdf = True

    except BaseException as e:
        pass

    return add_mail_pdf

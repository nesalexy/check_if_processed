import email
import os
import threading
import time
from concurrent.futures import ThreadPoolExecutor
from tempfile import NamedTemporaryFile

from googleapiclient import discovery
from tnefparse import TNEF

from db.db import DbBox
from google_drive.DriveBox import DriveBox
from main import Utils, Constants
from main.Constants import PREFIX_FOLDER
from main.FileWorker import FileWorker
from main.HashWorker import HashFileWorker
from main.MailMainCompany import MailMainCompany
from main.MailWorker import MailWorker, ImapMailConnect

# oopen mail folder for check email
from main.XmslBox import XmslBox
from pdf.PDFManager import PDFManager

FOLDER_PROCESSED = 'Processed'
FOLDER_FAILED = 'Failed'
MOVIE_TO_FOLDER ='Check_if_processed'


class MainWorker:

    def __init__(self, user, password):
        print("MailBox __init__")
        self.drive = DriveBox()
        self.hash_worker = HashFileWorker()
        self.file_worker = FileWorker()
        self.db_box = DbBox()
        self.mail_main_company = MailMainCompany()
        self.count_mail = 0

        self.xmsl_box = XmslBox()
        self.white_list = self.xmsl_box.get_while_list()

        self.imap_worker = ImapMailConnect(user, password)
        self.imap_worker.__enter__()
        self.mail_worker = MailWorker(self.imap_worker.imap)

        self.folders_list = self.get_folders_gd()

        self.folders_list_id = self.generation_list_folders_id(self.folders_list)
        self.hash_list = self.get_all_files(self.folders_list_id)
        print("size hash summ " + str(len(self.hash_list)))

        self.imap_worker = ImapMailConnect(user, password)
        self.imap_worker.__enter__()
        self.mail_worker = MailWorker(self.imap_worker.imap)

    def get_all_files(self, folders_list_id):
        print("all folders " + str(len(folders_list_id)))
        hash_sum = []
        for folder_id in folders_list_id:
            list_files = self.drive.list_files_from_parent_folder(folder_id)
            hash_sum.extend(self.create_hash_summ_list(list_files))

        return hash_sum

    def generation_list_folders_id(self, folders_list):
        folders_list_id = []

        for company in folders_list:
            try:
                main_company_name = Utils.get_main_company(company)
                company_prefix_name = Utils.get_main_company_prefix(company)
                if company_prefix_name is None:
                    print("get file list from " + main_company_name)
                    try:
                        gd_folder = self.drive.get_folder_by_name(main_company_name + Constants.PREFIX_FOLDER)
                        if gd_folder:
                            work_folder_id = gd_folder['id']
                            if work_folder_id:
                                folders_list_id.extend(self.get_all_folders_from_root_folder(work_folder_id))
                        else:
                            print("cant find folder " + main_company_name)

                    except BaseException as e:
                        print(company)
                        print(e)
                else:
                    print("prefix " + company_prefix_name + " in company " + main_company_name)
            except BaseException as e:
                print("WARNING " + str(e) + " " + str(company))

        return folders_list_id

    def get_all_folders_from_root_folder(self, work_folder_id):
        try:
            company_folder_list = []
            folder_list = self.drive.get_all_folder_from_folder(work_folder_id)

            company_folder_list.append(work_folder_id)

            if folder_list:
                for folder in folder_list:
                    self.get_folder(company_folder_list, folder)

            return company_folder_list
        except BaseException as e:
            print("generation_list_files " + str(e))
            raise Exception

    def get_folder(self, company_folder_list, folder):
        try:
            print(folder['title'])
            folder_list = self.drive.get_all_folder_from_folder(folder['id'])

            company_folder_list.append(folder['id'])

            if folder_list:
                for folder in folder_list:
                    self.get_folder(company_folder_list, folder)

        except BaseException as e:
            print("get_folder " + str(e))
            raise Exception

    def get_folders_gd(self):
        name_of_folders = []
        from main.Constants import DATE_MAIL_START
        from main.AttachmentManagerDel import AttachmentManager

        self.imap_worker.imap.select(FOLDER_FAILED)
        status, response_failed = self.imap_worker.imap.search(None, DATE_MAIL_START)
        failed_email_id = self.gen_failed_id(response_failed)

        self.imap_worker.imap.select(FOLDER_PROCESSED)
        status, response = self.imap_worker.imap.search(None, DATE_MAIL_START)

        if status == 'OK':
            items = response[0].split()
            print('start check mails ' + str(len(items)))
            if len(items) > 0:
                for email_id in reversed(items):
                    status, response = self.imap_worker.imap.fetch(email_id, "(RFC822)")
                    if status == 'OK':
                        mail = self.mail_worker.get_mail(response)
                        id = mail['Message-Id']
                        if id not in failed_email_id:
                            try:
                                company_name = self.get_main_name(mail)
                                if company_name not in name_of_folders:
                                    name_of_folders.append(company_name)
                            except BaseException as e:
                                print(e)

        self.imap_worker.__exit__()
        return name_of_folders

    # start main process
    def start_processing_mail(self):
        from main.Constants import DATE_MAIL_START
        from main.AttachmentManagerDel import AttachmentManager

        self.imap_worker.imap.select(FOLDER_FAILED)
        status, response_failed = self.imap_worker.imap.search(None, DATE_MAIL_START)
        failed_email_id = self.gen_failed_id(response_failed)

        self.imap_worker.imap.select(FOLDER_PROCESSED)
        status, response = self.imap_worker.imap.search(None, DATE_MAIL_START)

        if status == 'OK':
            items = response[0].split()
            print('start check mails ' + str(len(items)))
            if len(items) > 0:
                for email_id in reversed(items):
                    try:
                            status, response = self.imap_worker.imap.fetch(email_id, "(RFC822)")
                            if status == 'OK':

                                mail = self.mail_worker.get_mail(response)
                                id = mail['Message-Id']
                                if id not in failed_email_id:
                                    if mail.get_content_maintype() == 'multipart':

                                            attachment_manager = AttachmentManager(mail, self.hash_worker, self.hash_list, self.drive)
                                            if attachment_manager.is_movie_mail:
                                                self.mail_worker.move_message_to_another_folder(email_id, MOVIE_TO_FOLDER)

                                else:
                                   print('in failed')

                                self.count_mail += 1
                                print(str(self.count_mail))
                                print('----')

                    except BaseException as e:
                        print("start_processing_mail " + str(e))
                        print('----')

        self.imap_worker.__exit__()


    def create_hash_summ_list(self, list):
        # print('create_hash_summ_list')
        try:
            hash_summ_lsit = []
            for item in list:
                checksum = item.get('md5Checksum', 'no checksum')
                hash_summ_lsit.append(checksum)
                # self.db_box.add_new_hash(checksum)

            return hash_summ_lsit
        except BaseException as e:
            print("create_hash_summ_list " + str(e))

    def gen_failed_id(self, response_failed):
        try:
            failed_email_id = []
            items = response_failed[0].split()
            for email_id in reversed(items):
                status, response = self.imap_worker.imap.fetch(email_id, "(RFC822)")
                mail = self.mail_worker.get_mail(response)
                id = mail['Message-Id']
                failed_email_id.append(id)
            return failed_email_id
        except BaseException as e:
            print("gen_failed_id " + str(e))


    def get_main_name(self, mail):
        try:
            main_company_name = ""
            main_company_name_o = Utils.get_main_company(Utils.get_mail_delivered_to(mail))
            try:
                reciples = self.mail_main_company.get_list_mails(mail, self.white_list)
                main_company_name = Utils.get_main_company(reciples[0])
            except BaseException as e:
                main_company_name = main_company_name_o

            print(main_company_name + "@bokoredo.pro")
            print('----')
            return main_company_name + "@bokoredo.pro"

        except BaseException as e:
            print("get_work_folder_id " + str(e))
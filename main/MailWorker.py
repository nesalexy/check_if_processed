import datetime
import email
import imaplib
import re

from main.Constants import FOLDER_CHECK_IF_PROCESSED
from main.utils import Utils

# IMAP_SERVER = 'imap.gmail.com'
# IMAP_PORT = '993'
IMAP_USE_SSL = True
# IMAP_SERVER = 'mx.bokoredo.pro'
# IMAP_PORT = '993'
# IMAP_USE_SSL = True

# create lable, set label whith this name
FOLDER_PARTICALY_PROCESSED = 'partially_processed'

class ImapMailConnect:

    def __init__(self, user, password):
        self.user = user
        self.password = password

        if IMAP_USE_SSL:
            self.imap = imaplib.IMAP4_SSL('imap.gmail.com', '993')
        else:
            self.imap = imaplib.IMAP4('imap.gmail.com', '993')

    def __enter__(self):
        print("MailBox __enter__")
        self.imap.login(self.user, self.password)
        return self

    def __exit__(self):
        self.imap.close()
        self.imap.logout()


class MailWorker:

    def __init__(self, imap=None):
        self.ADDR_PATTERN = re.compile('<(.*?)>')
        self.utils = Utils()
        self.imap = imap

    def move_message_to_another_folder(self, email_id, copy_to_folder):
        status, response = self.imap.fetch(email_id, "(RFC822)")
        if status == 'OK':
            try:
                email_body = response[0][1].decode('utf-8')
                mail = email.message_from_string(email_body)
                result = self.imap.store(email_id, '+X-GM-LABELS', copy_to_folder)
                mov, data = self.imap.uid('STORE', email_id, '+FLAGS', '(\Deleted)')
                self.imap.expunge()
                print('movie mail to processed')

            except AttributeError:
                print('AttributeError')

    # decide where move mail
    def decide_move_folder_mail(self, status_exist_in_gd, email_id):
        count_status = self.utils.count_status(status_exist_in_gd)

        if count_status == 0:
            # print('not exist attachments ' + str(attachment_names))
            self.move_message_to_another_folder(email_id, FOLDER_CHECK_IF_PROCESSED)
            # pass
        elif count_status == len(status_exist_in_gd):
            pass  # all attachments exist
        else:
            # print('some attachment exist ' + str(attachment_names))
            self.move_message_to_another_folder(email_id, FOLDER_PARTICALY_PROCESSED)
            # pass

    def get_mail(self, response):
        try:
            email_body = response[0][1].decode('utf8', 'ignore').encode("ascii", errors="ignore")
            mail = email.message_from_bytes(email_body)
            return mail
        except BaseException as e:
            print("get_mail " + str(e))
            email_body = response[0][1]
            mail = email.message_from_bytes(email_body)
            return mail

    def get_date_from_mail(self, date):
        date_tuple = email.utils.parsedate_tz(date)
        date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
        return (datetime.datetime.strptime(str(date),
                                           '%Y-%m-%d %H:%M:%S')
            .strftime('%d-%b %H:%M'))
